#include <SPI.h> 
#define a1 6
#define a2 7
#define b1 8
#define b2 9
#define stb1 2
#define stb2 3

int pulsewidth = 5;  //delay time for latch
int heatwidth = 670;     // in Microseconds
int printingSpeed = 2;  // it's actually delay in Microseconds // if you give it less than 2 seconds, it's somehow too fast and the motor doesn't move.//

bool startPrint = true;

void setup()
{
    pinMode(a1, OUTPUT);
    pinMode(a2, OUTPUT);
    pinMode(b1, OUTPUT);
    pinMode(b2, OUTPUT);

    
  SPI.begin(); 
  SPI.setClockDivider(SPI_CLOCK_DIV8);
  SPI.setDataMode(SPI_MODE0);
  pinMode (stb1, OUTPUT);
  pinMode (stb2, OUTPUT);

  //init
  digitalWrite(SS, HIGH);
  digitalWrite(stb1, LOW);
  digitalWrite(stb2, LOW);
}

void loop(){
  if (startPrint) {
    line1();
    printDots();
    paperout();   
    
     for (int j=0; j<15; j++){
      paperout();
     }
    startPrint = false;
  }
}

void line1(){
  digitalWrite(SS, HIGH);
 
  SPI.transfer(0xFF, 0x00, 0xFF, 0x00, 0xFF, 0x00, 0xFF, 0x00);   //strobe 1
  SPI.transfer(0xFF, 0x00, 0xFF, 0x00, 0xFF, 0x00, 0xFF, 0x00);   //strobe 2  
  SPI.transfer(0xFF, 0x00, 0xFF, 0x00, 0xFF, 0x00, 0xFF, 0x00);   //strobe 3
  SPI.transfer(0xFF, 0x00, 0xFF, 0x00, 0xFF, 0x00, 0xFF, 0x00);   //strobe 4
  SPI.transfer(0xFF, 0x00, 0xFF, 0x00, 0xFF, 0x00, 0xFF, 0x00);   //strobe 5
  SPI.transfer(0xFF, 0x00, 0xFF, 0x00, 0xFF, 0x00, 0xFF, 0x00);   //strobe 6

  digitalWrite(SS, LOW);
  delayMicroseconds(pulsewidth);
  digitalWrite(SS, HIGH);  
}


void printDots(){
  digitalWrite(stb1, HIGH);
  digitalWrite(stb2, HIGH);
  delayMicroseconds(heatwidth);
  digitalWrite(stb1, LOW);
  digitalWrite(stb2, LOW);
}

void paperout()
{   //step4
    digitalWrite(a1, LOW);
    digitalWrite(a2, HIGH);
    digitalWrite(b1, LOW);
    digitalWrite(b2, HIGH);
    delay(printingSpeed); 
    //step3
    digitalWrite(a1, LOW);
    digitalWrite(a2, HIGH);
    digitalWrite(b1, HIGH);
    digitalWrite(b2, LOW);
    delay(printingSpeed);
    //step2
    digitalWrite(a1, HIGH);
    digitalWrite(a2, LOW);
    digitalWrite(b1, HIGH);
    digitalWrite(b2, LOW);
    delay(printingSpeed);
    //step1
    digitalWrite(a1, HIGH);
    digitalWrite(a2, LOW);
    digitalWrite(b1, LOW);
    digitalWrite(b2, HIGH);
    delay(printingSpeed);
}
